<?php

	class abastecimientoView{
		
		public function nuevo_abastecimiento($cache, $usuarios, $copiadoras, $paginacion){
			template()->buildFromTemplates('templatesystem.html');
			template()->addTemplateBit('contenido','nuevo_abastecimiento.html');
			page()->setTitle('Administración - Abastecimiento');
			page()->addEstigma('lista_fotocopiadora',array('SQL',$cache[0]));
			page()->addEstigma('registros',array('SQL',$cache[1]));
			page()->addEstigma('ftc',array('SQL',$cache[2]));
			page()->addEstigma('paginacion', $paginacion);

			foreach ($usuarios as $usuario) {
				page()->addEstigma('u_'.$usuario['id'], $usuario['correo']);
			}

			foreach ($copiadoras as $copiadora) {
				page()->addEstigma('f_'.$copiadora['id'], $copiadora['nombre']);
			}
			
			@template()->parseOutput();
			template()->parseExtras();

			print page()->getContent();
		}

		public function pedidos_pendientes($cache, $usuarios, $copiadoras, $paginacion){
			template()->buildFromTemplates('templatesystem.html');
			template()->addTemplateBit('contenido','pendientes.html');
			page()->setTitle('Fotocopiadora - Abastecimiento');
			page()->addEstigma('registros',array('SQL',$cache[0]));
			page()->addEstigma('paginacion', $paginacion);

			foreach ($usuarios as $usuario) {
				page()->addEstigma('u_'.$usuario['id'], $usuario['correo']);
			}

			foreach ($copiadoras as $copiadora) {
				page()->addEstigma('f_'.$copiadora['id'], $copiadora['nombre']);
			}
			@template()->parseOutput();
			template()->parseExtras();

			print page()->getContent();
		}
	}
?>