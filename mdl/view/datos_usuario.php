<?php

	class datos_usuarioView{

		public function nuevos_datos_usuario($datos){
			template()->buildFromTemplates('templatedatos.html');
			template()->addTemplateBit('contenido','nuevos_datos_usuario.html');
			page()->setTitle('Usuarios - Registro de datos del usuario');
			page()->addEstigma('idusuario',$datos['idusuario']);
			@template()->parseOutput();
			template()->parseExtras();
			print page()->getContent();
		}

	}
?>