<?php

class systemView{
	
	public function iniciar_sesion(){
		template()->buildFromTemplates('template.html');
		page()->setTitle('Inicio');
		template()->addTemplateBit('contenido','info.html');
		@template()->parseOutput();
		template()->parseExtras();
		print page()->getContent();
	}

	public function registro(){
		template()->buildFromTemplates('template.html');
		page()->setTitle('Iniciar sesion');
		template()->addTemplateBit('contenido','iniciar_sesion.html');
		@template()->parseOutput();
		template()->parseExtras();
		print page()->getContent();	
	}

	public function principal($acceso, $copias, $datos_f, $usadas, $dia, $correo, $id){
		if($acceso==3){
			template()->buildFromTemplates('templateuser.html');
			page()->setTitle('Inicio');
			template()->addTemplateBit('contenido','principal.html');
			page()->addEstigma('n_copias', $copias);	
			page()->addEstigma('n_copias_u', $usadas);
			page()->addEstigma('correo', $correo);
			page()->addEstigma('id', $id);
			$dia_str = "";
			switch ($dia) {
				case 1:
					$dia_str = "Lunes";
					break;
				case 2:
					$dia_str = "Martes";
					break;
				case 3:
					$dia_str = "Miércoles";
					break;
				case 4:
					$dia_str = "Jueves";
					break;
				case 5:
					$dia_str = "Viernes";
					break;
				case 6:
					$dia_str = "Sábado";
					break;
				case 7:
					$dia_str = "Domingo";
					break;					
				default:
					# code...
					break;
			}
			page()->addEstigma('dia', $dia_str);
		}else if($acceso==1){
			template()->buildFromTemplates('templatesystem.html');
			page()->setTitle('Inicio - Admin');
			template()->addTemplateBit('contenido','admin.html');
			template()->addTemplateBit('contenidoDinamico', "admin_default.html");
		}else if($acceso==2){
			template()->buildFromTemplates('templatesystem.html');
			page()->setTitle('Inicio - Fotocopiadora');
			template()->addTemplateBit('contenido','fotocopiadora.html');
			page()->addEstigma('n_copias', $copias);
			if($datos_f!=null){
				page()->addEstigma('encargado', $datos_f['encargado']);
				page()->addEstigma('direccion', $datos_f['direccion']);
				page()->addEstigma('nombre', $datos_f['nombre']);
				page()->addEstigma('telefono', $datos_f['telefono']);
				page()->addEstigma('pendientes', $datos_f['pendientes']);
			}
		}else{

		}

		
		@template()->parseOutput();
		template()->parseExtras();
		print page()->getContent();
	}

	public function parametros($cache){
		template()->buildFromTemplates('templatesystem.html');
		template()->addTemplateBit('contenido','configuracion.html');
		page()->setTitle('Configuración');
		page()->addEstigma('system', array('SQL', $cache[0]));
		@template()->parseOutput();
		template()->parseExtras();
		print page()->getContent();
	}
}

?>