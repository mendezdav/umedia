<?php

	class control_copiaView {

		public function nuevo_despacho($cache, $paginacion, $copiadoras){
			template()->buildFromTemplates('templatesystem.html');
			template()->addTemplateBit('contenido','nuevo_despacho.html');
			page()->setTitle('Control de Copias - Nuevo despacho');
			page()->addEstigma('lista_transacciones',array('SQL',$cache[0]));
			page()->addEstigma('paginacion',$paginacion);
			foreach ($copiadoras as $copiadora) {
				page()->addEstigma('f_'.$copiadora['id'],$copiadora['nombre']);
			}
			@template()->parseOutput();
			template()->parseExtras();
			print page()->getContent();
		}
	}
?>