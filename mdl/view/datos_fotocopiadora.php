<?php

	class datos_fotocopiadoraView{

		public function nuevo_datos_fotocopiadora($cache){
			template()->buildFromTemplates('templatesystem.html');
			template()->addTemplateBit('contenido','nuevo_datos_fotocopiadora.html');
			page()->setTitle('Administración - Registro de fotocopiadora');
			page()->addEstigma('lista_fotocopiadora',array('SQL',$cache[0]));
			@template()->parseOutput();
			template()->parseExtras();
			print page()->getContent();
		}
		public function modificar_datos_fotocopiadora($cache){
			template()->buildFromTemplates('templatesystem.html');
			template()->addTemplateBit('contenido','actualizar_datos_fotocopiadora.html');
			page()->setTitle('Administración - Actualización de fotocopiadora');
			page()->addEstigma('lista_fotocopiadora',array('SQL',$cache[0]));
			@template()->parseOutput();
			template()->parseExtras();
			print page()->getContent();
		}
	}
?>