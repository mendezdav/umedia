<?php

	class usuarioView{

		public function nuevo_usuario(){
			template()->buildFromTemplates('templatesystem.html');
			template()->addTemplateBit('contenido','nuevo_usuario.html');
			page()->setTitle('Usuarios - Nuevo usuario');
			@template()->parseOutput();
			template()->parseExtras();
			print page()->getContent();
		}

		public function usuario_fotocopiadora($cache){
			template()->buildFromTemplates('templatesystem.html');
			template()->addTemplateBit('contenido','nuevo_usuario_fc.html');
			page()->setTitle('Usuarios - fotocopiadoras');
			page()->addEstigma('fotocopiadoras', array('SQL', $cache[0]));
			page()->addEstigma('empleado_copiadora', array('SQL', $cache[1]));
			@template()->parseOutput();
			template()->parseExtras();
			print page()->getContent();	
		}

		public function editar($correo, $nombre, $rol){
			template()->buildFromTemplates('templatesystem.html');
			template()->addTemplateBit('contenido','cuenta.html');
			page()->setTitle('Usuario - Editar');
			page()->addEstigma('correo', $correo);
			page()->addEstigma('nombre', $nombre);
			page()->addEstigma('rol', $rol);
			@template()->parseOutput();
			template()->parseExtras();
			print page()->getContent();
		}

		public function estadisticas($cache, $total){
			template()->buildFromTemplates('templatesystem.html');
			template()->addTemplateBit('contenido','estadisticas.html');
			page()->setTitle('Usuarios - Estadisticas');
			foreach ($cache as $campo=>$reg) {
				page()->addEstigma($campo, array('SQL', $cache[$campo]));	
			}
			page()->addEstigma('s_0','Masculino');
			page()->addEstigma('s_1','Femenino');
			page()->addEstigma('total',$total);
			@template()->parseOutput();
			template()->parseExtras();
			print page()->getContent();
		}

		public function lista($cache, $paginacion){
			template()->buildFromTemplates('templatesystem.html');
			template()->addTemplateBit('contenido','lista.html');
			page()->setTitle('Usuarios - Código de barras');
			page()->addEstigma('usuario', array('SQL', $cache[0]));
			page()->addEstigma('paginacion',$paginacion);
			@template()->parseOutput();
			template()->parseExtras();
			print page()->getContent();
		}
	}
?>