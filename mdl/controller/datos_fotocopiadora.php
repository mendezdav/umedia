<?php

	import('mdl.view.datos_fotocopiadora');
	import('mdl.model.datos_fotocopiadora');

	class datos_fotocopiadoraController extends controller{

		public function nuevo_datos_fotocopiadora(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			$cache[0]=$this->model->get_list();
			$this->view->nuevo_datos_fotocopiadora($cache);
		}

		public function guardar_datos_fotocopiadora(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			if(isset($_POST) && !empty($_POST)):
				$this->model->get(0);
				$this->model->change_status($_POST);
				$this->model->save();
				HttpHandler::redirect('/umedia/datos_fotocopiadora/nuevo_datos_fotocopiadora');
			else:
				echo "acceso restringido";
			endif;
		}
		public function editar_datos_fotocopiadora(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			$id_categoria=isset($_GET['id'])?$_GET['id']:'0';
			$cache=array();
			$cache[0]=$this->model->search('id',$id_categoria);
			$this->view->modificar_datos_fotocopiadora($cache);
		}
		public function eliminar_datos_fotocopiadora(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			$id=isset($_GET['id'])?$_GET['id']:'0';
			$this->model->delete($id);	
			HttpHandler::redirect("/umedia/datos_fotocopiadora/nuevo_datos_fotocopiadora");
		}

		public function actualizar_datos_fotocopiadora(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			if(isset($_POST) && !empty($_POST)):
				$id=isset($_GET['id'])?$_GET['id']:0;
				$this->model->get($id);
				$this->model->change_status($_POST);
				$this->model->save();
				HttpHandler::redirect('/umedia/datos_fotocopiadora/nuevo_datos_fotocopiadora');
			else:
				echo "llamada realizada fuera de la funcion actualizar";
			endif;
		}
	}
?>