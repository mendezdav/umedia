<?php

	import('mdl.view.datos_usuario');
	import('mdl.model.datos_usuario');

	class datos_usuarioController extends controller{
		/**
		 * Verifica el estado de primera vista y da lugar al formulario
		 * de preferencias que solo se visualiza y llena una sola vez
		 * @return null 
		 */
		public function nuevos_datos_usuario(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			$datos = verificar_primera_vista();
			if($datos['primera_vista']==true):
				#primera vez pasamos a mostrar formulario
				$this->view->nuevos_datos_usuario($datos);
			else:
				HttpHandler::redirect('/umedia/system/principal');
			endif;
		}

		public function guardar_datos_usuario(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			if(isset($_POST) && !empty($_POST)):
				#necesito saber el id del registro asi que lo busco
				$idusuario=$_POST['idusuario'];
				$query="SELECT id FROM datos_usuario WHERE idusuario='$idusuario'";
				data_model()->executeQuery($query);
				$id_datos_usuario=data_model()->getResult()->fetch_assoc();
				$this->model->get($id_datos_usuario['id']);
				$this->model->change_status($_POST);
				$this->model->save();
				/*Cambiamos el estado de la primera_vista*/
				$query="UPDATE usuario SET primera_vista='0' WHERE id='$idusuario'";
				data_model()->executeQuery($query);
				HttpHandler::redirect('/umedia/system/principal');
			else:
				echo "Acceso restringido";
			endif;
		}
	}
?>