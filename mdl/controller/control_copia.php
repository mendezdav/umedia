<?php

	import('mdl.view.control_copia');
	import('mdl.model.control_copia');

	class control_copiaController extends controller{

		public function nuevo_despacho(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			#el id de la fotocopiadora
			$usuario = Session::getUser();
			import('scripts.paginacion');

			$query="SELECT empleado_copiadora.id_copiadora FROM empleado_copiadora INNER JOIN usuario ON empleado_copiadora.id_usuario=usuario.id
			WHERE usuario.correo='$usuario'";
			data_model()->executeQuery($query);
			$data =data_model()->getResult()->fetch_assoc();
			$id_copiadora =$data['id_copiadora'];
			$mdl = $this->model->get_sibling('transacciones');
			
			$numeroRegistros = $mdl->MultyQuantify(array('fotocopiadora'=>$id_copiadora), true);
			$url_filtro = "/umedia/control_copia/nuevo_despacho?";
			list($paginacion_str, $limitInf, $tamPag) = paginar($numeroRegistros, $url_filtro);
			$cache[0] = $mdl->Multyfilter(array('fotocopiadora'=>$id_copiadora), $limitInf, $tamPag,array('field'=>'id', 'type'=>'DESC'), true);
			$copiadoras = $this->model->get_sibling('datos_fotocopiadora')->get_list_array();
			$this->view->nuevo_despacho($cache, $paginacion_str, $copiadoras);
		}

		public function guardar_despacho(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			$datos_form=$_POST;#cantidad y id_usuario (estudiante)
			/**
			 * Buscamos copias disponibles, id de fotocopiadora, id del usuario (fotocopiadora)
			 * 
			 */
			$usuario=Session::getUser();
			
			$query="SELECT datos_fotocopiadora.id, datos_fotocopiadora.copias_disponibles,
			usuario.id,	empleado_copiadora.id_usuario, empleado_copiadora.id_copiadora
			FROM empleado_copiadora INNER JOIN usuario ON empleado_copiadora.id_usuario=usuario.id INNER JOIN 
			datos_fotocopiadora ON empleado_copiadora.id_copiadora=datos_fotocopiadora.id 
			WHERE usuario.correo='$usuario'";
			data_model()->executeQuery($query);
			$info=data_model()->getResult()->fetch_assoc();
			if($datos_form['cantidad']<0) $datos_form['cantidad'] = $datos_form['cantidad'] * -1;
			$query="SELECT control_copia.copias_disponibles, control_copia.copias_usadas, control_copia.correo FROM control_copia INNER JOIN usuario ON control_copia.correo =usuario.correo
			WHERE usuario.id=".$datos_form['id_usuario'];
			data_model()->executeQuery($query);
			$copias_usuario_disponibles=data_model()->getResult()->fetch_assoc();

			echo var_dump($info);
			echo "<br/><br/>";
			echo var_dump($datos_form);
			echo "<br/><br/>";
			echo var_dump($copias_usuario_disponibles);
			/*
			 * Hay suficientes copias en la fotocopiadora?
			 * El estudiante tiene disponibles esa cantidad?
			 */
			if($info['copias_disponibles']< $datos_form['cantidad']){
				#NO
				HttpHandler::redirect('/umedia/control_copia/nuevo_despacho?error=insuficientes_fotocopiadora');
			}
				
			else if($datos_form['cantidad']> $copias_usuario_disponibles['copias_disponibles']){
				HttpHandler::redirect('/umedia/control_copia/nuevo_despacho?error=insuficientes_estudiante');
			}else{
				#En este punto se considera que si se cumplen todos los aspectos necesarios para insertar este registro
				#debemos insertar en transacción y actualizar en control_copia
				$control_copia_disponibles = $copias_usuario_disponibles['copias_disponibles'] - $datos_form['cantidad'];
				$control_copia_usadas      = $datos_form['cantidad']+$copias_usuario_disponibles['copias_usadas'];
				$control_copia_fecha= date('Y-m-d');
				$control_copia_correo= $copias_usuario_disponibles['correo'];
				$query="UPDATE control_copia SET copias_disponibles='$control_copia_disponibles' , copias_usadas=$control_copia_usadas
				, ultima_fecha='$control_copia_fecha' WHERE correo='$control_copia_correo'";
				data_model()->executeQuery($query);

				#Finalizo el update de control_copia ahora registramos la transacción en la respectiva tabla
				$transaccion['correo']=$control_copia_correo; #metemos el correo del usuario mismo ocupado en el anterior
				$transaccion['fecha']=$control_copia_fecha;#la misma fecha del punto anterior
				$transaccion['fotocopiadora']=$info['id_copiadora']; #array que posee muchos datos entre ellos el id de la fotocopiadora
				$transaccion['cantidad']=$datos_form['cantidad']; #numero de elementos que se ingresaron

				$model_transaccion=$this->model->get_sibling('transacciones');
				$model_transaccion->get(0);
				$model_transaccion->change_status($transaccion);
				$model_transaccion->save();

				$msd = $this->model->get_sibling('datos_fotocopiadora');
				$msd->get($info['id_copiadora']);
				$msd->set_attr('copias_disponibles', $msd->get_attr('copias_disponibles') - $datos_form['cantidad']);
				$msd->save();
				
				HttpHandler::redirect('/umedia/control_copia/nuevo_despacho?success=ok');
			}
		}
	}
?>