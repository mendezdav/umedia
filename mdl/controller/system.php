<?php

import('mdl.model.system');
import('mdl.view.system');

class systemController extends controller{

	public function inicio(){
		if(Session::ValidateSession()){
			
            HttpHandler::redirect('/umedia/system/principal');
		}else{

			$this->view->iniciar_sesion();
		}
	}

    public function registro(){
        
        $this->view->registro();
    }

	public function iniciar_sesion(){
		if (empty($_POST)) {
            HttpHandler::redirect('/umedia/system/inicio');
        } else {
            $correo  = data_model()->sanitizeData($_POST['correo']);
            $clave   = md5($_POST['clave']);
            $query   = "SELECT * FROM usuario WHERE correo='{$correo}' AND clave='{$clave}';";
            data_model()->executeQuery($query);
            if (data_model()->getNumRows() > 0) {
                $level = 3;
                while ($data = data_model()->getResult()->fetch_assoc()) {
                    $level = $data['acceso'];
                }
                Session::singleton()->NewSession($correo, $level);
                HttpHandler::redirect('/umedia/system/principal');
            } else {
                HttpHandler::redirect('/umedia/system/inicio?error=2');
            }
        }
	}

	public function principal(){
		if(!Session::ValidateSession()){
			HttpHandler::redirect('/umedia/system/inicio');
		}
        $primera_vista=verificar_primera_vista();
        if($primera_vista['primera_vista']==true):
            HttpHandler::redirect('/umedia/datos_usuario/nuevos_datos_usuario');
        else:
            $copias = 0;
            $acceso = Session::getLevel();
            $correo = Session::getUser();
            $datos_f = null;
            $usadas = 0;
            $dia = 0;
            $id  = 0;

            if($acceso == 3){

                $ctl = $this->model->get_child('control_copia');
                $ctl->setVirtualId('correo');
                $this->model->get(1);
                $umdl = $this->model->get_sibling('usuario');
                $umdl->setVirtualId('correo');
                $umdl->get($correo);
                $id = $umdl->get_attr('id');
                if(!$ctl->exists($correo)){
                    $copias_system = $this->model->get_attr('cantidad_copias');
                    
                    $ctl->setVirtualId('id');
                    $ctl->get(0); 
                    $ctl->set_attr('correo', $correo);
                    $ctl->set_attr('copias_disponibles', $copias_system);
                    $ctl->set_attr('copias_usadas', '0');
                    $ctl->save();
                }

                $dia = $this->model->get_attr('dia_referencia');
                $ctl->setVirtualId('correo');
                $ctl->get($correo);
                $copias = $ctl->get_attr('copias_disponibles');
                $usadas = $ctl->get_attr('copias_usadas');
            }

            if($acceso == 2){
                $usuario = Session::getUser();
                $umdl = $this->model->get_sibling('usuario');
                $umdl->setVirtualId('correo');
                $umdl->get($usuario);
                $id = $umdl->get_attr('id');

                $obd = $this->model->get_child('empleado_copiadora');
                $obd->setVirtualId('id_usuario');
                $obd->get($id);

                $copiadora = $obd->get_attr('id_copiadora');

                $cp = $this->model->get_sibling('datos_fotocopiadora');
                $cp->get($copiadora);
                $copias = $cp->get_attr('copias_disponibles');
                $datos_f = array();
                $datos_f['encargado'] = $cp->get_attr('encargado');
                $datos_f['direccion'] = $cp->get_attr('direccion');
                $datos_f['nombre']    = $cp->get_attr('nombre');
                $datos_f['telefono']  = $cp->get_attr('telefono');

                $query = "SELECT count(*) pendientes FROM abastecimiento WHERE fotocopiadora=$copiadora AND anulado = 0 AND confirmado = 0";
                data_model()->executeQuery($query);
                $response = data_model()->getResult()->fetch_assoc();
                $datos_f['pendientes'] = $response['pendientes'];
            }

            $this->view->principal($acceso, $copias, $datos_f, $usadas, $dia, $correo, $id);
        endif;

	}

    public function parametros(){
        if(!Session::ValidateSession()){
            HttpHandler::redirect('/umedia/system/inicio');
        }
        $cache = array();
        $cache[0] = $this->model->get_list();
        $this->view->parametros($cache);
    }

    public function guardar_configuracion(){
        if(!Session::ValidateSession()){
            HttpHandler::redirect('/umedia/system/inicio');
        }
        if(isset($_POST) && !empty($_POST)){
            $this->model->get(1);
            $this->model->change_status($_POST);
            $this->model->save();
            HttpHandler::redirect('/umedia/system/parametros?status=ok');
        }
    }

}

?>