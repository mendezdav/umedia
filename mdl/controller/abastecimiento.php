<?php

	import('mdl.view.abastecimiento');
	import('mdl.model.abastecimiento');

	class abastecimientoController extends controller{

		public function nuevo_abastecimiento(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			#Para abastecer necesitamos ver las fotocopiadoras
			import('scripts.paginacion');
			$m_datos_fotocopiadora=$this->model->get_sibling('datos_fotocopiadora');
			$cache[0] = $m_datos_fotocopiadora->get_list();

			if(isset($_GET['filtro']) && !empty($_GET['filtro'])){
				$filtro = data_model()->sanitizeData($_GET['filtro']);
				$tipo_filtro = (isset($_GET['tipo_filtro']) && !empty($_GET['tipo_filtro'])) ? data_model()->sanitizeData($_GET['tipo_filtro']) : 'id';
				$numeroRegistros = $this->model->quantify($tipo_filtro, $filtro);
            	$url_filtro = "/umedia/abastecimiento/nuevo_abastecimiento?filtro=" . $filtro . "&";
            	list($paginacion_str, $limitInf, $tamPag) = paginar($numeroRegistros, $url_filtro);
            	$cache[1] = $this->model->filter($tipo_filtro, $filtro, $limitInf, $tamPag,array('field'=>'fecha_hora', 'type'=>'DESC'));
			}else{
				$numeroRegistros = $this->model->quantify();
            	$url_filtro = "/umedia/abastecimiento/nuevo_abastecimiento?";
            	list($paginacion_str, $limitInf, $tamPag) = paginar($numeroRegistros, $url_filtro);
            	$cache[1] = $this->model->get_list($limitInf, $tamPag, null, array('field'=>'fecha_hora', 'type'=>'DESC'));
			}
			
			$cache[2] = $m_datos_fotocopiadora->get_list();

			$usuarios = $this->model->get_sibling('usuario')->get_list_array();
			$copiadoras = $this->model->get_sibling('datos_fotocopiadora')->get_list_array();

			$this->view->nuevo_abastecimiento($cache, $usuarios, $copiadoras, $paginacion_str);
		}

		public function abastecer(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			if(isset($_POST) && !empty($_POST)){
				#array que tendra los datos a almacenar
				$data=array();
				$data=$_POST;#guardamos id de ftcp y la cantidad
				
				if($data['cantidad']!=intval($data['cantidad'])) {$data['cantidad'] = 0; }

				if($data['cantidad']!=0){

					if($data['cantidad']<0){

						$data['cantidad'] = $data['cantidad'] * - 1;	
					}

					#el id del usuario que hace la transacción
					$correo=Session::getUser();
					$query="SELECT id FROM usuario WHERE correo='$correo'";
					data_model()->executeQuery($query);
					$row=data_model()->getResult()->fetch_assoc();
					$data['usuario']=$row['id'];
					$data['anulado']='0';
					$data['anulado_por']=$row['id'];
					$data['fecha_hora']=date('Y-m-d H:i:s');



					$this->model->get(0);
					$this->model->change_status($data);
					$this->model->save();

					/**
					 * A este punto ya tenemos el registro almacenado de abastecimiento
					 * ahora necesitamos actualizar datos_fotocopiadora para proporcionar 
					 * el nuevo numero de copias
					 */
					/*$id=$_POST['fotocopiadora'];
					$query="SELECT copias_disponibles FROM datos_fotocopiadora WHERE id='$id'";
					data_model()->executeQuery($query);
					$row=data_model()->getResult()->fetch_assoc();
					$cantidad=$row['copias_disponibles'];  
					
					$cantidad=$cantidad+$data['cantidad'];#nueva cantidad 
					$query="UPDATE datos_fotocopiadora SET copias_disponibles='$cantidad' WHERE id='$id'";
					data_model()->executeQuery($query);*/
					
				}

				HttpHandler::redirect('/umedia/abastecimiento/nuevo_abastecimiento');
			}else{
				echo "acceso restringido";
			}
		}

		public function pedidos_pendientes(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}

			import('scripts.paginacion');

			$correo = Session::getUser();
			$user   = $this->model->get_sibling('usuario'); 

			$user->setVirtualId('correo');
			$user->get($correo);

			$iduser = $user->get_attr('id');

			$empc = $this->model->get_child('empleado_copiadora');
			$empc->setVirtualId('id_usuario');
			$empc->get($iduser);

			$copiadora = $empc->get_attr('id_copiadora');

			$m_datos_fotocopiadora=$this->model->get_sibling('datos_fotocopiadora');
			$numeroRegistros = $this->model->MultyQuantify(array('fotocopiadora'=>$copiadora, 'anulado'=>'0', 'confirmado'=>'0'), true);
            $url_filtro = "/umedia/abastecimiento/nuevo_abastecimiento?";
            list($paginacion_str, $limitInf, $tamPag) = paginar($numeroRegistros, $url_filtro);
            $cache[0] = $this->model->Multyfilter(array('fotocopiadora'=>'1', 'anulado'=>'0', 'confirmado'=>'0'), $limitInf, $tamPag,array('field'=>'fecha_hora', 'type'=>'DESC'), true);

			$usuarios = $this->model->get_sibling('usuario')->get_list_array();
			$copiadoras = $this->model->get_sibling('datos_fotocopiadora')->get_list_array();

			$this->view->pedidos_pendientes($cache, $usuarios, $copiadoras, $paginacion_str);
		}

		public function anular(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}

			$id = (isset($_GET['id']) && !empty($_GET['id']))? $_GET['id']: 0;
			if($this->model->exists($id)){
				$this->model->get($id);
				$confirmado = $this->model->get_attr('confirmado');
				$anulado = $this->model->get_attr('anulado');
				if($confirmado==1){
					HttpHandler::redirect('/umedia/abastecimiento/nuevo_abastecimiento?error=confirmado');
				}else{
					if($anulado==1){
						HttpHandler::redirect('/umedia/abastecimiento/nuevo_abastecimiento?error=anulado');
					}else{
						$this->model->set_attr('anulado', '1');
						$this->model->save();
						$copiadora = $this->model->get_attr('fotocopiadora');
						$cantidad  = $this->model->get_attr('cantidad');
						$df = $this->model->get_sibling('datos_fotocopiadora');
						$df->get($copiadora);
						$df->set_attr('copias_disponibles', $df->get_attr('copias_disponibles') - $cantidad);
						$df->save();
						HttpHandler::redirect('/umedia/abastecimiento/nuevo_abastecimiento?exito=anulacion');
					}
				}
			}else{
				HttpHandler::redirect('/umedia/abastecimiento/nuevo_abastecimiento?error=404');
			}
		}

		public function confirmar(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			$id = (isset($_GET['id']) && !empty($_GET['id']))? $_GET['id']: 0;
			if($this->model->exists($id)){
				$this->model->get($id);
				$confirmado = $this->model->get_attr('confirmado');
				$anulado    = $this->model->get_attr('anulado');
				$cantidad   = $this->model->get_attr('cantidad');
				$copiadora  = $this->model->get_attr('fotocopiadora');
				if($anulado==1){
					HttpHandler::redirect('/umedia/abastecimiento/pedidos_pendientes?error=anulado');
				}else{
					if($confirmado==1){
						HttpHandler::redirect('/umedia/abastecimiento/pedidos_pendientes?error=confirmado');
					}else{
						$this->model->set_attr('confirmado', '1');
						$this->model->save();
						$ftc = $this->model->get_sibling('datos_fotocopiadora');
						$ftc->get($copiadora);
						$ftc->set_attr('copias_disponibles', $ftc->get_attr('copias_disponibles') + $cantidad);
						$ftc->save();
						HttpHandler::redirect('/umedia/abastecimiento/pedidos_pendientes?exito=anulacion');
					}
				}
			}else{
				HttpHandler::redirect('/umedia/abastecimiento/pedidos_pendientes?error=404');
			}
		}
	}
?>