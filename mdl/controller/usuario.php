<?php

	import('mdl.view.usuario');
	import('mdl.model.usuario');

	class usuarioController extends controller{

		/**
		 * [nuevo_usuario description]
		 * @return [type] [description]
		 */
		public function nuevo_usuario(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			$this->view->nuevo_usuario();
		}
		/**
		 * [guardar_usuario description]
		 * @return [type] [description]
		 */
		public function guardar_usuario(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			if(isset($_POST) && !empty($_POST)):
				//md5
				$datos = $_POST;
				$datos['clave']  = md5($datos['clave']);
				$datos['clave2'] = md5($datos['clave2']);
				$datos['acceso'] = 3;
				$datos['primera_vista']="1";
				#primera visita es 1
				try{
					$id_original = $this->model->getId();
					$this->model->setVirtualId('correo');
					if($this->model->exists($datos['correo'])){
						HttpHandler::redirect('/umedia/system/registro?error=extuser');	
					}else{
						if($datos['clave']==$datos['clave2']){
							$this->model->setVirtualId($id_original);
							$this->model->get(0);
							$this->model->change_status($datos);
							$this->model->save();
							/*Iniciamos la sesión*/
							Session::singleton()->NewSession($datos['correo'], $datos['acceso']);
							/*
							 * Pasamos a guardar la otra información que pertenece a porción muy
							 * pequeña de los datos de datos_usuario
							 */
							$model_datos_usuario=$this->model->get_sibling('datos_usuario');#modulo necesario
							$query="SELECT MAX(id) FROM usuario";#ultimo id
							data_model()->executeQuery($query);
							$id=data_model()->getResult()->fetch_assoc();
							/*Guardamos los datos en la otra tabla
							* Datos a guardar: nombre completo, id(fk) y fecha
							*/
							$model_datos_usuario->guardar_datos_iniciales($datos,$id);
							/*Como es primera vez pasamos al formulario de registro*/
							HttpHandler::redirect('/umedia/system/registro?status=ok');
						}else{
							HttpHandler::redirect('/umedia/system/registro?error=passmissmatch');		
						}
					}
				}catch(Exception $e){
					HttpHandler::redirect('/umedia/system/registro?error=500');	
				}
			else:
				echo "Acceso restringido";
			endif;
		}

		public function guardar_usuario_copiadora(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			if(isset($_POST) && !empty($_POST)):
				//md5
				$datos = $_POST;
				$datos['clave']  = md5($datos['clave']);
				$datos['clave2'] = md5($datos['clave2']);
				$datos['acceso'] = 2;
				$datos['primera_vista']="0";
				#primera visita es 0
				try{
					$id_original = $this->model->getId();
					$this->model->setVirtualId('correo');
					if($this->model->exists($datos['correo'])){
						HttpHandler::redirect('/umedia/usuario/usuario_fotocopiadora?error=extuser');	
					}else{
						if($datos['clave']==$datos['clave2']){
							$this->model->setVirtualId($id_original);
							$this->model->get(0);
							$this->model->change_status($datos);
							$this->model->save();
							/*Iniciamos la sesión*/
							Session::singleton()->NewSession($datos['correo'], $datos['acceso']);
							/*
							 * Pasamos a guardar la otra información que pertenece a porción muy
							 * pequeña de los datos de datos_usuario
							 */
							$model_datos_usuario=$this->model->get_sibling('datos_usuario');#modulo necesario
							$query="SELECT MAX(id) FROM usuario";#ultimo id
							data_model()->executeQuery($query);
							$id=data_model()->getResult()->fetch_assoc();
							/*Guardamos los datos en la otra tabla
							* Datos a guardar: nombre completo, id(fk) y fecha
							*/
							$model_datos_usuario->guardar_datos_iniciales($datos,$id);
							/*Como es primera vez pasamos al formulario de registro*/

							$up = $this->model->get_child('empleado_copiadora');
							$up->get(0);
							$up->set_attr('id_usuario', $id['MAX(id)']);
							$up->set_attr('id_copiadora', $datos['fotocopiadora']);
							$up->save();
							HttpHandler::redirect('/umedia/usuario/usuario_fotocopiadora?status=ok');
						}else{
							
							HttpHandler::redirect('/umedia/usuario/usuario_fotocopiadora?error=passmissmatch');		
						}
					}
				}catch(Exception $e){
					HttpHandler::redirect('/umedia/usuario/usuario_fotocopiadora?error=500');	
				}
			else:
				echo "Acceso restringido";
			endif;
		}

		public function usuario_fotocopiadora(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			$cache = array();
			$cache[0] = $this->model->get_sibling('datos_fotocopiadora')->get_list();
			$query="SELECT usuario.correo, datos_fotocopiadora.nombre FROM empleado_copiadora INNER JOIN usuario ON usuario.id =empleado_copiadora.id_usuario
			INNER JOIN datos_fotocopiadora ON datos_fotocopiadora.id=empleado_copiadora.id_copiadora";

			$cache[1] = data_model()->cacheQuery($query);
			$this->view->usuario_fotocopiadora($cache);
		}

		public function cargar_datos(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			if(isset($_POST['id_usuario']) && !empty($_POST['id_usuario'])){
				$id = $_POST['id_usuario'];
				$response = array();
				$response['message'] = null;

				if($this->model->exists($id)){

					$this->model->get($id);
					if($this->model->get_attr('acceso')!=3){
						$response['message'] = "No existe el usuario";	
					}else{
						$correo = $this->model->get_attr('correo');
						$ob = $this->model->get_sibling('control_copia');
						$ob->setVirtualId('correo');
						$ob->get($correo);
						$ft = $this->model->get_sibling('datos_usuario');
						$ft->setVirtualId('idusuario');
						$ft->get($id);
						$response['nombre'] = $ft->get_attr('primer_nombre')." ".$ft->get_attr('primer_apellido');
						$response['copias_disponibles'] = $ob->get_attr('copias_disponibles');
						$response['correo'] = $correo;
					}
				}else{
					
					$response['message'] = "No existe el usuario";
				}

				echo json_encode($response);	
			}else{

				echo "Error!";
			}
			
		}

		public function editar(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			$correo = Session::getUser();
			$this->model->setVirtualId('correo');
			$this->model->get($correo);
			$iduser = $this->model->get_attr('id');
			$user   = $this->model->get_sibling('datos_usuario');
			$user->setVirtualId('idusuario');
			$user->get($iduser);
			$nombre = $user->get_attr('primer_nombre')." ".$user->get_attr('segundo_nombre')." ".$user->get_attr('primer_apellido')." ".$user->get_attr('segundo_apellido');
			$rol = "";
			switch (Session::getLevel()) {
				case 1:
					$rol = "Administrador";
					break;
				case 2:
					$rol = "Empleado fotocopiadora";
					break;	
				case 3:
					$rol = "Afiliado";
					break;
				default:
					# code...
					break;
			}
			$this->view->editar($correo, $nombre, $rol);
		}

		public function cambiar_password(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			if(isset($_POST) && !empty($_POST)){
				$actual = $_POST['actual'];
				$nueva = $_POST['nueva'];
				$repeticion = $_POST['repeticion'];

				$correo = Session::getUser();
				$this->model->setVirtualId('correo');
				$this->model->get($correo);
				$clave = $this->model->get_attr('clave');

				if(md5($actual)!=$clave){
					HttpHandler::redirect('/umedia/usuario/editar?error=nomatch');					
				}else{
					if($nueva != $repeticion){
						HttpHandler::redirect('/umedia/usuario/editar?error=missmatch');	
					}else{
						if($actual == $nueva){
							HttpHandler::redirect('/umedia/usuario/editar?error=same');	
						}else{
							if(strlen($nueva)<6){
								HttpHandler::redirect('/umedia/usuario/editar?error=length');	
							}else{
								$this->model->set_attr('clave', md5($nueva));
								$this->model->save();
								HttpHandler::redirect('/umedia/usuario/editar?status=ok');
							}
						}
					}
				} 

			}else{
				echo "Error!";
			}
		}

		public function estadisticas(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			$query = "SELECT $, COUNT($) as cantidad FROM datos_usuario WHERE ($!=' ' OR $='0') AND idusuario IN (SELECT id FROM usuario WHERE acceso=3) GROUP BY $";
			$campos = array('sexo', 
				'marca_ropa',
				'marca_zapatos', 
				'marca_cosmeticos', 
				'departamento', 
				'ciudad', 
				'universidad', 
				'carrera',
				'tipo_transporte',
				'compania_telefono',
				'comida_rapida',
				'nombre_banco',
				'tipo_cuenta',
				'pasatiempo_1',
				'pasatiempo_2',
				'pasatiempo_3');

			$cache = array();

			foreach ($campos as $campo) {
				$nquery  = str_replace('$', $campo, $query);
				$cache[$campo] = data_model()->cacheQuery($nquery);
			}

			$query = "SELECT count(*) total FROM datos_usuario WHERE idusuario IN (SELECT id FROM usuario WHERE acceso=3); ";
			data_model()->executeQuery($query);
			$res = data_model()->getResult()->fetch_assoc();
			$this->view->estadisticas($cache, $res['total']);
		}

		public function lista(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			#Para abastecer necesitamos ver las fotocopiadoras
			import('scripts.paginacion');

			$numeroRegistros = $this->model->quantify("acceso", "3");
            $url_filtro = "/umedia/usuario/lista?";
            list($paginacion_str, $limitInf, $tamPag) = paginar($numeroRegistros, $url_filtro);
            $cache[0] = $this->model->filter("acceso", "3", $limitInf, $tamPag);

            $this->view->lista($cache, $paginacion_str);

		}

		public function codigo_barra(){
			if(!Session::ValidateSession()){
				HttpHandler::redirect('/umedia/system/inicio');
			}
			if(isset($_GET['id'])&& !empty($_GET['id'])){
				$id = $_GET['id'];
				if($this->model->exists($id)){
					$len = strlen("$id");
					for($i = $len; $i < 7; $i++){
						$id = "0".$id;
					}

					require_once(APP_PATH.'common\plugins\barcodegen\class\BCGFontFile.php');
					require_once(APP_PATH.'common\plugins\barcodegen\class\BCGColor.php');
					require_once(APP_PATH.'common\plugins\barcodegen\class\BCGDrawing.php');
					include_once(APP_PATH.'common\plugins\barcodegen\class\BCGcode39.barcode.php');

					// The arguments are R, G, and B for color.
					$colorFront = new BCGColor(0, 0, 0);
					$colorBack = new BCGColor(255, 255, 255);
					$font = new BCGFontFile(APP_PATH.'common\plugins\barcodegen\font\Arial.ttf', 18);

					$code = new BCGcode39(); // Or another class name from the manual
					$code->setScale(2); // Resolution
					$code->setThickness(30); // Thickness
					$code->setForegroundColor($colorFront); // Color of bars
					$code->setBackgroundColor($colorBack); // Color of spaces
					$code->setFont($font); // Font (or 0)
					$code->parse($id); // Text

					$drawing = new BCGDrawing('', $colorBack);
					$drawing->setBarcode($code);
					$drawing->draw();

					header('Content-Type: image/png');
					$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
				}
			}
		}
	}
?>