<?php

session_start();

//error_reporting(E_ALL);

define("BUSINESS_MANAGER", false); # aplicacion en produccion

require_once('settings/settings.php');  # carga configuraciones
require_once('scripts/encryption.php'); # funciones de proteccion de datos

import('core.bm_engine');

date_default_timezone_set(TIMEZONE);

import('core.engine.database');
import('core.engine.template');
import('core.engine.page');
import('core.engine.frontController');
import('core.handler.http');
import('core.handler.controller');
import('core.handler.object');
import('core.handler.sessionHandler');
import('core.handler.MysqliHandler');
import('core.handler.debugHandler');
import('core.orm.helper');


import('scripts.pdf.fpdf');
import('scripts.pdf.report');
import('scripts.alias');

import('mdl.error');

#####	configuraciones iniciales	#####


BM::singleton()->storeObject('template', 'temp');
BM::singleton()->storeObject('database', 'db');
BM::singleton()->storeSetting('default', 'skin');

BM::singleton()->getObject('temp')->getPage()->setJs('static/js/jquery.min.js');
BM::singleton()->getObject('temp')->getPage()->setCss('static/css/style.css');
BM::singleton()->getObject('temp')->getPage()->setCss('static/css/reset.css');
BM::singleton()->getObject('temp')->getPage()->setCss('static/css/layout.css');


BM::singleton()->getObject('temp')->getPage()->setJs('static/js/business.manager.1.0.js');
BM::singleton()->getObject('temp')->getPage()->setJs('static/js/jquery.min.js');
BM::singleton()->getObject('temp')->getPage()->setJs('static/js/cufon-yui.js');
BM::singleton()->getObject('temp')->getPage()->setJs('static/js/cufon-replace.js');
BM::singleton()->getObject('temp')->getPage()->setJs('static/js/Molengo_400.font.js');
BM::singleton()->getObject('temp')->getPage()->setJs('static/js/Expletus_Sans_400.font.js');


BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, DATABASE);

if(Session::ValidateSession())
	verificacion_copias();

#####	fin de configuraciones #####

$front = new frontController(array());  # crear controlador 'front'
$front->run();        # correr controlador 'front'
exit();
?>